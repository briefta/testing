package com.tester.demo;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

@Repository
public class ExecutorTestor {

	@Autowired
    @Qualifier("fixedThreadPool")
	ExecutorService executor;
	
	Logger log = Logger.getLogger("tester");
	public List<String> dates(){
		List<Callable<String>> calls = new ArrayList<Callable<String>>();
		long start = System.currentTimeMillis();
		List<String> datas = new ArrayList<String>();
		for( int a=0;a<1000;a++) {
			calls.add(gett("String "+a,datas));
					}
        try {
			List<Future<String>> futures = executor.invokeAll(calls);
			
			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
      
        long stop = System.currentTimeMillis();
        log.info((stop-start)/1000 +" secs");
		return datas;
		
	}
	
	
	
	public Callable<String> gett(String data,List<String> dats){
		Callable<String> yee = new Callable<String>() {
			String d = data;
			@Override
			public String call() throws Exception {
				// TODO Auto-generated method stub
				d = data+LocalDate.now().toString();
				 dats.add(d);
				 Thread.sleep(10);
				return d;
			}
		};
		return yee;
	}
	
	
}
