package com.tester.services;

import org.jvnet.hk2.annotations.Service;
import org.springframework.security.core.userdetails.User.UserBuilder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.tester.demo.User;

@Service
public class UserDetailsServiceImp implements UserDetailsService{

    
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		User user = findUserbyUername(username);
		 UserBuilder builder = null;
		    if (user != null) {
		      builder = org.springframework.security.core.userdetails.User.withUsername(username);
		      builder.password(new BCryptPasswordEncoder().encode(user.getPassword()));
		      builder.roles(user.getRoles());
		    } else {
		      throw new UsernameNotFoundException("User not found.");
		    }

		    return builder.build();
	
	}

	
	
	private User findUserbyUername(String username) {
	    if(username.equalsIgnoreCase("admin")) {
	    	String[] d = {"ADMIN","USER"};
	      return new User(1, "pda.kpg", username, "1212132", d );
	    }
	    return null;
	  }
}
